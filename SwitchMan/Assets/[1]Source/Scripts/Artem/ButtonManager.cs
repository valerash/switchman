﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public Button moveLeft;
    public Button moveRight;
    public Button moveUp;
    public Button moveDown;
    public Button jumpBth;

    private void Awake()
    {
        moveUp.gameObject.SetActive(false);
        moveDown.gameObject.SetActive(false);
    }
}
