﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;

    private float hMoveInput;
    private float vMoveInput;

    public int maxJumps;
    private int extraJumps;

    private bool flipX = false;

    private Rigidbody2D rb;
    private ButtonManager buttons;

    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask isGround;

    public bool isUnderWater;

    #region Functions

    public void DoJump()
    {
        if (extraJumps > 0)
        {
            rb.velocity = Vector2.up * jumpSpeed;
            extraJumps--;
        }
    }

    private void Flip()
    {
        flipX = !flipX;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    #endregion

    #region OnTrigger

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject colObj = collision.gameObject;
        if (colObj.layer == 4)
        {
            isUnderWater = true;
            buttons.jumpBth.gameObject.SetActive(false);
            buttons.moveUp.gameObject.SetActive(true);
            buttons.moveDown.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GameObject colObj = collision.gameObject;
        if (colObj.layer == 4)
        {
            isUnderWater = false;
            buttons.jumpBth.gameObject.SetActive(true);
            buttons.moveUp.gameObject.SetActive(false);
            buttons.moveDown.gameObject.SetActive(false);
        }
    }

    #endregion

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        buttons = GameObject.FindObjectOfType<ButtonManager>();
        extraJumps = maxJumps;
    }

    private void Update()
    {
        //check if player is grounded
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, isGround);
    }

    void FixedUpdate()
    {
        //reset jumps if player is grounded
        if (isGrounded)
        {
            extraJumps = maxJumps;
        }

        //horizontal move
        hMoveInput = CrossPlatformInputManager.GetAxis("Horizontal");
        rb.velocity = new Vector2(hMoveInput * speed, rb.velocity.y);

        //if player is in water
        if (isUnderWater)
        {
            vMoveInput = CrossPlatformInputManager.GetAxis("Vertical");
            if (System.Convert.ToBoolean(vMoveInput))
                rb.velocity = new Vector2(rb.velocity.x, vMoveInput * speed);
        }

        //flip on x axis
        if (System.Convert.ToBoolean(hMoveInput))
        {
            if ((flipX && hMoveInput > 0) || (!flipX && hMoveInput < 0))
            {
                Flip();
            }
        }
    }
}
