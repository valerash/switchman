﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixeye.Framework;
using UnityEngine.SceneManagement;

public class CameraController : MonoCached, ITickFixed
{
    public GameObject follow;

    protected override void Setup()
    {
        ProcessorUpdate.Add(this);
    }

    void OnEnable()
    {
        follow = GameObject.FindWithTag("Player");
    }

    public void TickFixed(float delta)
    {
        var newX = Mathf.Lerp(gameObject.transform.position.x, follow.transform.position.x, 0.3f);
        var newY = Mathf.Lerp(gameObject.transform.position.y, follow.transform.position.y, 0.3f);
        gameObject.transform.position = new Vector3(newX, newY, gameObject.transform.position.z);
    }
}
